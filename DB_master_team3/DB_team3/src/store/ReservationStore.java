package store;

import java.util.List;

import domain.Airline.AirlineTicket;
import domain.Airline.Airplane;
import domain.Airline.Airplaneroute;
import domain.Customer.CustomerSeat;
import domain.Customer.Myreservation;
import domain.Customer.Reservation;


public interface ReservationStore {

	Reservation readdeleteuser(String customerId);
	Reservation readselectuser(String customerId);
	List<Airplaneroute> ReservationList(String startPoint, String endPoint);
	List<AirlineTicket> MyReservationList(String ID);
	Airplaneroute reservation(String routeno);
	String seat(String seatno);
	List<CustomerSeat> readseatall();
	Myreservation readmylist(String seatnumber, String routeno);
	Myreservation readmyflist(Myreservation mylist, String customerid, String airnumber);
	Myreservation mypage(String customerid, String seatNumber);
	Myreservation readtomypage(String userid);
	Airplaneroute readairtemp(String routeno);
	Airplane readairplane(String airname, String time);


	Myreservation readmyreservationid(String reservationno);
	Myreservation readremypage(String reservationnumber);
	List<Myreservation> readmyreadlist(String userid);
	Myreservation readremove(String reservationno);
}
