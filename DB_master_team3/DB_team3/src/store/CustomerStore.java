package store;

import domain.Airline.Airplaneroute;
import domain.Customer.CustomerInformation;

public interface CustomerStore {
	CustomerInformation findUserId(String customerId);
	CustomerInformation registerUser(CustomerInformation customer);
	void insertReservation(Airplaneroute air, String seatno, String userid);
	void DeleteUser(String userid);
	void ModifyUser(CustomerInformation customer);

}
