package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import domain.Airline.Airplaneroute;
import domain.Customer.CustomerInformation;
import store.CustomerStore;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;

@Repository
public class CustomerStoreLogic implements CustomerStore{

	private ConnectionFactory connectionFactory;

	public CustomerStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public CustomerInformation findUserId(String customerId) {
		String sql = "SELECT userid, userpass, username, userage, usersex, phone, usernum, passportnum, address FROM user WHERE userid= ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		CustomerInformation customeridcheak = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, customerId);

			rs = psmt.executeQuery();

			if (rs.next()) {
				customeridcheak = new CustomerInformation();
				customeridcheak.setCustomerId(rs.getString("userid"));
				customeridcheak.setCustomerPass(rs.getString("userpass"));
				customeridcheak.setCustomerName(rs.getString("username"));
				customeridcheak.setCustomerAge(rs.getInt("userage"));
				customeridcheak.setCustomerSex(rs.getString("usersex"));
				customeridcheak.setCustomerNumber(rs.getString("phone"));
				customeridcheak.setCustomerSSNumber(rs.getString("usernum"));
				customeridcheak.setPassportNumber(rs.getString("passportnum"));
				customeridcheak.setCustomeraddr(rs.getString("address"));
			}	
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return customeridcheak;
	}

	@Override
	public CustomerInformation registerUser(CustomerInformation customer) {	
		
		String sql = "insert into user(userid, userpass, username, userage, usersex, phone, usernum, passportnum, address) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
		
			psmt.setString(1, customer.getCustomerId());
			psmt.setString(2, customer.getCustomerPass());
			psmt.setString(3, customer.getCustomerName());
			psmt.setInt(4, customer.getCustomerAge());
			psmt.setString(5, customer.getCustomerSex());
			psmt.setString(6, customer.getCustomerNumber());
			psmt.setString(7, customer.getCustomerSSNumber());
			psmt.setString(8, customer.getPassportNumber());
			psmt.setString(9, customer.getCustomeraddr());
			psmt.executeUpdate();
			
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return customer;
	}

	@Override
	public void insertReservation(Airplaneroute air, String seatno, String userid) {
		Connection conn = null;
	      PreparedStatement psmt = null;
	      ResultSet rs = null;

	      
	      int tempno = 1001;
	      
	      
	      try {
	         String sql = "SELECT reservationno FROM reservation ORDER BY reservationno DESC";
	         conn = connectionFactory.createConnection();
	         psmt = conn.prepareStatement(sql);
	         rs = psmt.executeQuery();
	         if (rs.next()) {
	            tempno = rs.getInt(1) + 1;
	         }
	         JdbcUtils.close(psmt, conn, rs);
	         String sql2 = "INSERT INTO reservation (reservationno, boardingtime, startpoint, finishpoint, airlinename, routeno, userid, seatno)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	         conn = connectionFactory.createConnection();
	         psmt = conn.prepareStatement(sql2);
	         
	         psmt.setInt(1, tempno);
	         psmt.setString(2, air.getStartTime());
	         psmt.setString(3, air.getStartPoint());
	         psmt.setString(4, air.getEndPoint());
	         psmt.setString(5, air.getAirlineName());
	         psmt.setString(6, air.getAirplanerouteNumber());
	         psmt.setString(7, userid);
	         psmt.setString(8, seatno);
	         
	         psmt.executeUpdate();
	   
	      } catch (SQLException e) {
	         e.printStackTrace();
	      } finally {
	         JdbcUtils.close(conn,psmt);
	      }
		
	}

	@Override
	public void DeleteUser(String userid) {
		      String sql = "DELETE FROM user WHERE userid = ?";

		      Connection conn = null;
		      PreparedStatement psmt = null;
		      ResultSet rs = null;

		      try {
					conn = connectionFactory.createConnection();
					psmt = conn.prepareStatement(sql.toString());
					psmt.setString(1, userid); 
					psmt.executeUpdate();
				
		      } catch (SQLException e) {
		         throw new RuntimeException(e);
		      } finally {
		         JdbcUtils.close(rs, psmt, conn);
		      }
		   }

	public void ModifyUser(CustomerInformation customer) {
		String sql = "UPDATE user SET userpass = ?, username = ?, userage = ?, "
				+ " usersex = ?, phone = ?, usernum = ?, passportnum = ?, address = ? "
				+ " WHERE userid = ?";
	      Connection conn = null;
	      PreparedStatement pstmt = null;

	      try {
	         conn = connectionFactory.createConnection();
	         pstmt = conn.prepareStatement(sql);
	         
	         pstmt.setString(1, customer.getCustomerPass());
	         pstmt.setString(2, customer.getCustomerName());
	         pstmt.setInt(3, customer.getCustomerAge());
	         pstmt.setString(4, customer.getCustomerSex());
	         pstmt.setString(5, customer.getCustomerNumber());
	         pstmt.setString(6, customer.getCustomerSSNumber());
	         pstmt.setString(7, customer.getPassportNumber());
	         pstmt.setString(8, customer.getCustomeraddr());
	         pstmt.setString(9, customer.getCustomerId());
	         pstmt.executeUpdate();

	      } catch (SQLException e) {
	         throw new RuntimeException(e);
	      } finally {
	         JdbcUtils.close(pstmt, conn);
	      }
		
	}
}
