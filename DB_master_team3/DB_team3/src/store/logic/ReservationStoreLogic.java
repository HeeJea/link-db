package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Repository;

import domain.Airline.AirlineTicket;
import domain.Airline.Airplane;
import domain.Airline.Airplaneroute;
import domain.Customer.CustomerSeat;
import domain.Customer.Myreservation;
import domain.Customer.Reservation;
import store.ReservationStore;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;


@Repository
public class ReservationStoreLogic implements ReservationStore{

	private ConnectionFactory connectionFactory;

	public ReservationStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public Reservation readdeleteuser(String customerId) {

		String sql = "DELETE FROM USER where id=";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Reservation reservation = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, customerId.trim()); //怨듬갚?占쏙옙占�? 硫붿냼?占쏙옙 trim()
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return reservation;
	}


	@Override
	public Reservation readselectuser(String customerId) {
		// TODO Auto-generated method stub
		String sql = "SELECT -작성해야됨- WHERE userid= ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Reservation reservation = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, customerId);
			rs = psmt.executeQuery();

			if (rs.next()) {

				/*	reservation = new Reservation();
				reservation.setAirlineTicketId();
				reservation.setBoardTime();
				reservation.setCustomerId();
				reservation.setCustomerName();
				reservation.setEndPoint();
				reservation.setSeatNumber();
				reservation.setStartPoint();
				reservation.setVairlineName();
				 */
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return reservation;	}

	@Override
	public List<Airplaneroute> ReservationList(String startPoint, String endPoint) {
		String sql = "SELECT routeno, airlinename, stime, ftime, startpoint, finishpoint "
				+ " FROM route "
				+ " WHERE startpoint = ? AND finishpoint = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Airplaneroute> airlinelist = new ArrayList<Airplaneroute>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, startPoint);
			psmt.setString(2, endPoint);
			rs = psmt.executeQuery();
			while(rs.next()) { 

				Airplaneroute reservationlist  = new Airplaneroute();
				reservationlist.setAirplanerouteNumber(rs.getString("routeno"));
				reservationlist.setAirlineName(rs.getNString("airlinename"));
				reservationlist.setStartTime(rs.getString("stime"));
				reservationlist.setEndTime(rs.getNString("ftime"));
				reservationlist.setStartPoint(rs.getNString("startpoint"));
				reservationlist.setEndPoint(rs.getNString("finishpoint"));
				airlinelist.add(reservationlist);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return airlinelist;
	}

	@Override
	public List<AirlineTicket> MyReservationList(String ID) {
		String sql = "SELECT reservationno, boardingtime, airlinename, startpoint, finishpoint, routeno "
				+ " FROM reservation"
				+ " WHERE userid = ? ";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<AirlineTicket> Myairlinelist = new ArrayList<AirlineTicket>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, ID);
			rs = psmt.executeQuery();

			while(rs.next()) { 

				AirlineTicket myreservationlist  = new AirlineTicket();
				myreservationlist.setAirlineTicketId(rs.getNString("reservationno"));
				myreservationlist.setBoardTime(rs.getNString("boardingtime"));
				myreservationlist.setAirlineName(rs.getNString("airlinename"));
				myreservationlist.setStartPoint(rs.getNString("startpoint"));
				myreservationlist.setEndPoint(rs.getNString("finishpoint"));
				myreservationlist.setAirplaneNumber(rs.getNString("routeno"));
				Myairlinelist.add(myreservationlist);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return Myairlinelist;
	}

	@Override
	public Airplaneroute reservation(String routeno) {
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Airplaneroute res = null;

		try {
			String sql = "SELECT routeno, airlinename, stime, ftime, startpoint, finishpoint"
					+ " FROM route"
					+ " WHERE routeno = ?";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);

			psmt.setString(1, routeno);
			rs = psmt.executeQuery();

			while (rs.next()) {

				res = new Airplaneroute();
				res.setAirplanerouteNumber(rs.getString(1));
				res.setAirlineName(rs.getString(2));
				res.setStartTime(rs.getString(3));
				res.setEndTime(rs.getString(4));
				res.setStartPoint(rs.getString(5));
				res.setEndPoint(rs.getString(6));
			}


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn,psmt);
		}
		return res;
	}

	@Override
	public String seat(String seatno) {
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT seatclass"
					+ " FROM seat"
					+ " WHERE seatno = ?";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			psmt.setString(1, seatno);

			return rs.getString(1);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn,psmt,rs);
		}
		return null;
	}

	@Override
	public List<CustomerSeat> readseatall() {
		String sql = "SELECT seatno, price, seatclass FROM seat";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<CustomerSeat> seats = new ArrayList<CustomerSeat>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while(rs.next()) { 
				CustomerSeat seat  = new CustomerSeat();
				seat.setSeatNumber(rs.getString("seatno"));
				seat.setSeatPrice(rs.getInt("price"));
				seat.setSeatRank(rs.getString("seatclass"));
				seats.add(seat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return seats;
	}

	@Override
	public Myreservation readmylist(String seatnumber, String routeno) {

		System.out.println("-------------");
		System.out.println(seatnumber);
		System.out.println(routeno);
		System.out.println("-------------");

		Myreservation my = null;
		String sql = "select seatno, price, seatclass, routeno, airlinename, startpoint, finishpoint, stime, airportno from seat, route where seatno=? && routeno=?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, seatnumber);
			psmt.setString(2, routeno);
			rs = psmt.executeQuery();

			if(rs.next()) { 
				my  = new Myreservation();
				my.setBoardingtime(rs.getString("stime"));
				my.setStartpoint(rs.getString("startpoint"));
				my.setFinishpoint(rs.getString("finishpoint"));
				my.setAirlinename(rs.getString("airlinename"));
				my.setAirlineno(rs.getString("airportno"));
				my.setRouteno(rs.getString("routeno"));
				my.setSeatno(rs.getString("seatno"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return my;
	}


	@Override
	public Myreservation readmyflist(Myreservation mylist, String customerid, String airnumber) {
		String sql = "insert into reservation(reservationno, boardingtime, startpoint, finishpoint, airlinename, airplaneno, routeno, userid, seatno) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String num = RandomStringUtils.randomAlphanumeric(7);
		String num1 = RandomStringUtils.randomNumeric(7);
		

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());

			psmt.setString(1, (num + num1));
			psmt.setString(2, mylist.getBoardingtime());
			psmt.setString(3, mylist.getStartpoint());
			psmt.setString(4, mylist.getFinishpoint());
			psmt.setString(5, mylist.getAirlinename());
			psmt.setString(6, airnumber);
			psmt.setString(7, mylist.getRouteno());
			psmt.setString(8, customerid);
			psmt.setString(9, mylist.getSeatno());
			psmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return mylist;
	}

	@Override
	public Myreservation mypage(String customerid, String seatNumber) {
		String sql = "select * from reservation where userid = ? && seatno = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Myreservation my = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());		
			psmt.setString(1, customerid);
			psmt.setString(2, seatNumber);
			rs = psmt.executeQuery();
			if(rs.next())
			{
				my = new Myreservation();
				my.setReservationno(rs.getString("reservationno"));
//				my.setBoardingtime(rs.getString("boardingtime"));
//				my.setStartpoint(rs.getString("startpoint"));
//				my.setFinishpoint(rs.getString("finishpoint"));
//				my.setAirlinename(rs.getString("airlinename"));
//				my.setAirlineno(rs.getString("airplaneno"));
//				my.setRouteno(rs.getString("routeno"));
//				my.setUserid(rs.getString("userid"));
//				my.setSeatno(rs.getString("seatno"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return my;
	}

	@Override
	public Airplaneroute readairtemp(String routeno) {
		String sql = "select airlinename, stime from route where routeno = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Airplaneroute air = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, routeno);
			rs = psmt.executeQuery();
			if(rs.next())
			{
				air = new Airplaneroute();
				air.setAirlineName(rs.getString("airlinename"));
				air.setStartTime(rs.getString("stime"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return air;
	}


	@Override
	public Airplane readairplane(String airname, String time) {
		String sql = "select airplaneno from airplane where airlinename = ? && boardingtime= ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Airplane air = null;
		System.out.println(airname + time);
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, airname);
			psmt.setString(2, time);
			rs = psmt.executeQuery();
			if(rs.next())
			{
				air = new Airplane();
				air.setAirplaneNumber(rs.getString("airplaneno"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return air;
	}

	@Override
	public Myreservation readtomypage(String userid) {
			String sql = "select * from reservation where userid = ?";
			Connection conn = null;
			PreparedStatement psmt = null;
			ResultSet rs = null;
			Myreservation my = null;

			try {
				conn = connectionFactory.createConnection();
				psmt = conn.prepareStatement(sql.toString());
				psmt.setString(1, userid);
				rs = psmt.executeQuery();
				if(rs.next())
				{
					my = new Myreservation();
					my.setReservationno(rs.getString("reservationno"));
					my.setBoardingtime(rs.getString("boardingtime"));
					my.setStartpoint(rs.getString("startpoint"));
					my.setFinishpoint(rs.getString("finishpoint"));
					my.setAirlinename(rs.getString("airlinename"));
					my.setAirlineno(rs.getString("airplaneno"));
					my.setRouteno(rs.getString("routeno"));
					my.setUserid(rs.getString("userid"));
					my.setSeatno(rs.getString("seatno"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}finally {
				JdbcUtils.close(conn,rs,psmt);
			} 
			return my;
		}

	@Override
	public Myreservation readremypage(String reservationnumber) {
		String sql = "select * from reservation where reservationno = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Myreservation my = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());		
			psmt.setString(1, reservationnumber);
			rs = psmt.executeQuery();
			if(rs.next())
			{
				my = new Myreservation();
				my.setReservationno(rs.getString("reservationno"));
				my.setBoardingtime(rs.getString("boardingtime"));
				my.setStartpoint(rs.getString("startpoint"));
				my.setFinishpoint(rs.getString("finishpoint"));
				my.setAirlinename(rs.getString("airlinename"));
				my.setAirlineno(rs.getString("airplaneno"));
				my.setRouteno(rs.getString("routeno"));
				my.setUserid(rs.getString("userid"));
				my.setSeatno(rs.getString("seatno"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return my;
	}

	@Override
	public List<Myreservation> readmyreadlist(String userid) {
		
		String sql = "select * from reservation where userid = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Myreservation> mys = new ArrayList<Myreservation>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userid);
			rs = psmt.executeQuery();
			while(rs.next())
			{
				Myreservation my = new Myreservation();
				my.setReservationno(rs.getString("reservationno"));
				my.setBoardingtime(rs.getString("boardingtime"));
				my.setStartpoint(rs.getString("startpoint"));
				my.setFinishpoint(rs.getString("finishpoint"));
				my.setAirlinename(rs.getString("airlinename"));
				my.setAirlineno(rs.getString("airplaneno"));
				my.setRouteno(rs.getString("routeno"));
				my.setUserid(rs.getString("userid"));
				my.setSeatno(rs.getString("seatno"));
				mys.add(my);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return mys;
	}

	@Override
	public Myreservation readremove(String reservationno) {
		String sql = "DELETE FROM reservation where reservationno=?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Myreservation my = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, reservationno); 
			psmt.executeUpdate();
					}
		catch(SQLException e) {			
		throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return my;
	}

	@Override
	public Myreservation readmyreservationid(String reservationno) {
		String sql = "select * from reservation where reservationno = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Myreservation my = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());		
			psmt.setString(1, reservationno);
			rs = psmt.executeQuery();
			if(rs.next())
			{
				my = new Myreservation();
				my.setReservationno(rs.getString("reservationno"));
				my.setBoardingtime(rs.getString("boardingtime"));
				my.setStartpoint(rs.getString("startpoint"));
				my.setFinishpoint(rs.getString("finishpoint"));
				my.setAirlinename(rs.getString("airlinename"));
				my.setAirlineno(rs.getString("airplaneno"));
				my.setRouteno(rs.getString("routeno"));
				my.setUserid(rs.getString("userid"));
				my.setSeatno(rs.getString("seatno"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,rs,psmt);
		} 
		return my;
	}

}
