package controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import domain.Customer.CustomerInformation;
import domain.Customer.Myreservation;
import service.CustomerService;
import service.ReservationService;

@Controller
@RequestMapping("customer")
public class CustomerController {
	
	@Autowired
	private CustomerService Cservice;
	
	@Autowired
	private ReservationService service;
	
	
	@RequestMapping("main.do")
	public String main() {
		return "main"; 
	}
	
	@RequestMapping(value = "login.do", method = RequestMethod.GET)
	public String Login() {
		return "login";
	}
		
	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public String Login(HttpSession session, CustomerInformation customer, Model model) {
		
		System.out.println(customer.getCustomerId());
		System.out.println(customer.getCustomerPass());
		
		
		CustomerInformation customer1 = Cservice.login(customer.getCustomerId());
		
		session.setAttribute("customerid", customer1.getCustomerId());
		session.setAttribute("customername", customer1.getCustomerName());
		model.addAttribute("customer", this.getClass());

		return "redirect:/customer/main.do";		

	}

	@RequestMapping("mypagelist.do")
	public String mypagelist(Model model, HttpSession session, CustomerInformation customer) {
		String userid = (String)session.getAttribute("customerid");
		customer = Cservice.login(userid);
		model.addAttribute("user", customer);
		return "mypage";
	}
	
	
	@RequestMapping("userdelete.do")
	   public String Delete(HttpSession session) {
	      String userid = (String)session.getAttribute("customerid");
	      Cservice.DeleteUser(userid);
	      session.invalidate();
	      return "redirect:/customer/main.do";
	   }
	
	@RequestMapping(value = "modify.do", method = RequestMethod.GET)
	public String modify(Model model, HttpSession session, CustomerInformation customer) {
		String userid = (String)session.getAttribute("customerid");
		customer = Cservice.login(userid);
		model.addAttribute("user", customer);
		return "ModifyUser";
	}
	
	
	@RequestMapping(value = "modify.do", method = RequestMethod.POST)
	public String modify(HttpSession session,CustomerInformation customer, Model model) {
		
		String userid = (String) session.getAttribute("customerid");
		customer.setCustomerId(userid);
		
		Cservice.modify(customer);
		customer = Cservice.login(userid);
		model.addAttribute("user", customer);
		return "mypage";
	}

	@RequestMapping("redelete.do")
	public String Delete(String reservationno, Model model) {
		
		Myreservation my = service.myreservationid(reservationno);
		String myid = my.getUserid();
		service.remove(reservationno);
		
		return "redirect:/customer/myrelist.do?userid=" + myid;
		
	}

	
	@RequestMapping("myrelist.do")
	public String myrelist(Model model, String userid) {
		List<Myreservation> my = service.myreadlist(userid);

		System.out.println(my.size());
		model.addAttribute("myrelist", my);
		return "MyReservationlist";
	}

	
	@RequestMapping(value = "join.do", method = RequestMethod.GET)
	public String Join() {
		return "join";
	}
	
	
	@RequestMapping(value = "join.do", method = RequestMethod.POST)
	public String Join(Model model, CustomerInformation customer) {	
		model.addAttribute("joincustomer", Cservice.join(customer));
		return "redirect:/customer/main.do";		

	}
	
	@RequestMapping(value ="logout.do")
	public String logout(HttpSession session, Model model) {	
		session.invalidate();
		JOptionPane.showMessageDialog(null, "로그아웃 되었습니다.");
		return "redirect:/customer/main.do";
	}
	

	
}
