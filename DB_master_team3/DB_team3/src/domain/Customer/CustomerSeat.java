package domain.Customer;

public class CustomerSeat {

	private String seatNumber;
	private int seatPrice;
	private String seatRank;
	
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	public int getSeatPrice() {
		return seatPrice;
	}
	public void setSeatPrice(int seatPrice) {
		this.seatPrice = seatPrice;
	}
	public String getSeatRank() {
		return seatRank;
	}
	public void setSeatRank(String seatRank) {
		this.seatRank = seatRank;
	}

	
}
