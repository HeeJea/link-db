package domain.Customer;

public class CustomerInformation {

	private String customerId;
	private String customerPass;
	private String customerName;
	private int customerAge; 
	private String customerSex;  
	private String customerNumber; 
	private String customerSSNumber; 
	private String passportNumber; 
	private String customeraddr; 
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerPass() {
		return customerPass;
	}
	public void setCustomerPass(String customerPass) {
		this.customerPass = customerPass;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getCustomerAge() {
		return customerAge;
	}
	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}
	public String getCustomerSex() {
		return customerSex;
	}
	public void setCustomerSex(String customerSex) {
		this.customerSex = customerSex;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCustomerSSNumber() {
		return customerSSNumber;
	}
	public void setCustomerSSNumber(String customerSSNumber) {
		this.customerSSNumber = customerSSNumber;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getCustomeraddr() {
		return customeraddr;
	}
	public void setCustomeraddr(String customeraddr) {
		this.customeraddr = customeraddr;
	}
	
	

	
}
