package domain.Airline;

public class Airline {

	private String airlineName; // 비행기 번호
	private String country;
	private String homePage;
	private String airlineNumber;
	
	
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	public String getAirlineNumber() {
		return airlineNumber;
	}
	public void setAirlineNumber(String airlineNumber) {
		this.airlineNumber = airlineNumber;
	}


}
