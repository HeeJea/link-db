package domain.Airline;

public class AirlineTicket {

	private String airlineTicketId;
	private String boardTime;
	private String startPoint;
	private String endPoint;
	private String airlineName;
	private String airplaneNumber;
	private String customerId; 
	private String seatNumber;
	private String airplanerouteNumber;
	
	public String getAirlineTicketId() {
		return airlineTicketId;
	}
	public void setAirlineTicketId(String airlineTicketId) {
		this.airlineTicketId = airlineTicketId;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public String getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getAirplaneNumber() {
		return airplaneNumber;
	}
	public void setAirplaneNumber(String airplaneNumber) {
		this.airplaneNumber = airplaneNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	public String getAirplanerouteNumber() {
		return airplanerouteNumber;
	}
	public void setAirplanerouteNumber(String airplanerouteNumber) {
		this.airplanerouteNumber = airplanerouteNumber;
	}
	
	
	
}
