package service.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.Airline.Airplaneroute;
import domain.Customer.CustomerInformation;
import service.CustomerService;
import store.CustomerStore;

@Service
public class CustomerServiceLogic implements CustomerService{
	
	@Autowired
	private CustomerStore Cstore;
	
	@Override
	public CustomerInformation login(String customerId) {
		return Cstore.findUserId(customerId);
	}
	
	@Override
	public CustomerInformation join(CustomerInformation customer) {
		return Cstore.registerUser(customer);
	}

	@Override
	public void insertReservation(Airplaneroute air, String seatno, String userid) {
		Cstore.insertReservation(air, seatno, userid);
	}

	@Override
	public void DeleteUser(String userid) {
		Cstore.DeleteUser(userid);

	}
	@Override
	public void modify(CustomerInformation customer) {
		Cstore.ModifyUser(customer);
		
	}


}
