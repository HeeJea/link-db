package service;

import domain.Airline.Airplaneroute;
import domain.Customer.CustomerInformation;

public interface CustomerService {

	CustomerInformation login(String customerId);
	CustomerInformation join(CustomerInformation customer);
	void insertReservation(Airplaneroute air, String seatno, String userid);
	void DeleteUser(String userid);
	void modify(CustomerInformation customer);
}
