<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>좌석</title>
<%@ include file="/views/common.jsp"%>


</head>
<body>

   <div align="center">
    <form
         action="${pageContext.request.contextPath}/reservation/userreservation.do?routeno=${route.airplanerouteNumber}"
         method="post">
         
      <br> <br>
      <table class="table">
         <colgroup>
           <col width="200"/>
           <col width="200"/>
           <col width="200"/>
          </colgroup>
          <thead>
           <tr>
            <th class="text-center">좌석 번호</th>
            <th class="text-center">좌석 등급</th>
            <th class="text-center">예매</th>
           </tr>
          </thead>
         <tbody>
          <td class="text-center">
            <tr>
        	  <c:forEach var="list" items="${seatclass}">
         		 <tr>
         		 <td class="text-center">${list.seatNumber}</td>
         		  <td class="text-center">${list.seatRank}</td>
        		   <td class="text-center">
            	  <a href = "${pageContext.request.contextPath}/reservation/reservationfinal.do?seatNumber=${list.seatNumber}&&routeno=${route.airplanerouteNumber}">
            	  
            	  <input class="btn btn-success" type="button" value="${list.seatRank} 클레스 자리 예약"></a>
           </td>
   		  </tr>
          </c:forEach>
          </td>

         </tbody>
      </table>

   </div>
</body>
</html>