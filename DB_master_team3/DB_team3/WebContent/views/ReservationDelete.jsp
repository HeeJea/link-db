<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>회원가입</title>
<%@ include file="/views/common.jsp"%>
</head>
<body>
	<div style="padding: 100px">
		<hr>
		<br>
		<div align="center">
			<table class="table">
				<tr>
					<th>이름 :</th>
					<td>${Reservationlist.customerId}</td>
				</tr>
				<tr>
					<th>전화번호 :</th>
					<td>${Reservationlist.customerName}</td>
				</tr>
				<tr>
					<th>예매번호 :</th>
					<td>${Reservationlist.airlineTicketId}</td>
				</tr>
				<tr>
					<th>항공사 :</th>
					<td>${Reservationlist.vairlineName}</td>
				</tr>

				<tr>
					<th>출발지 :</th>
					<td>${Reservationlist.startPoint}</td>
				</tr>
				<tr>
					<th>도착지 :</th>
					<td>${Reservationlist.endPoint}</td>
				</tr>
				<tr>
					<th>탑승시간:</th>
					<td>${Reservationlist.boardTime}</td>
				</tr>
				<tr>
					<th>좌석 :</th>
					<td>${Reservationlist.seatNumber}</td>
				</tr>
			</table>
			<br>
			<th>삭제되었습니다.</th>
			<div align="center">
				<a href="customer/myreservation.do"><input class="btn btn-success" type="button"
					value="My 페이지로 가기"></a>
			</div>
			</form>
		</div>
	</div>
</body>
</html>