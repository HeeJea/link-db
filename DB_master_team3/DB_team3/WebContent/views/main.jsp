<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>메인</title>
<%@ include file="/views/common.jsp"%>

</head>
<body>

	<div align="center">

		<br> <br>
		<form
			action="${pageContext.request.contextPath}/reservation/reservationlist.do"
			method="post">
			<table class="table" align="center">
			<tr>
					<th>StartPoint</th>
					<td><input class="form-control" type="text" id="startPoint" name="startPoint"  placeholder="출발 공항을 입력하세요" value="" /></td>
			</tr>
			<tr>
					<th>EndPoint</th>

					<td><input class="form-control" type="text" id="endPoint" name="endPoint"  placeholder="도착 공항을 입력하세요" value=""/>
					</td>
			</tr>	</table>
			<br>
			<div align="center">
				<input class="btn" type="reset" value="취소"> <input
					class="btn btn-success" type="submit" value="예약리스트 확인하러가기">
			</div>
		</form>

		<section id="three">
			<div class="inner">
				<article>
					<div class="content">
						<span class="icon fa-laptop"></span>
						<header>
							<h3>학교 홈페이지 접속하기</h3>
						</header>
						<p>백석대학교(白石大學校, Baekseok University)는 1994년 3월, 학교법인 백석학원이 개교한 충청남도 천안시의 개신교 계열 4년제 대학이다.</p>
						<ul class="actions">
							<li><a href="#" target=“_blank" class="button alt">홈페이지 보러가기</a></li>
						</ul>
					</div>
				</article>
				<article>
					<div class="content">
						<span class="icon fa-diamond"></span>
						<header>
							<h3>항공정보포털시스템</h3>
						</header>
						<p>항공정보포털시스템은 산재된 항공관련 정보들을 한곳에 모아 필요한 항공정보를 신속히 제공하고 있는 시스템입니다.</p>
						<ul class="actions">
							<li><a href="#" class="button alt">항공정보포털시스템 보러가기</a></li>
						</ul>
					</div>
				</article>
				<article>
					<div class="content">
						<span class="icon fa-laptop"></span>
						<header>
							<h3>한국관광공사</h3>
						</header>
						<p>국가 경제발전과 국민복지증진에 기여 국민경제 발전에 기여를 목적으로 설립된 위탁집행형 준정부기관이다. 1962년 6월 26일 설립되었다.</p>
						<ul class="actions">
							<li><a href="#" class="button alt">한국관광사 보러가기</a></li>
						</ul>
					</div>
				</article>
			</div>
		</section>

		<!-- Footer -->
		<section id="footer">
			<div class="inner">
				<form method="post" action="#">
					<div class="footer">
						<ul>
							<li><a href="#">사이트 도움말</a></li>
							<li><a href="#">사이트 이용약관</a></li>
							<li><a href="#">사이트 운영규칙</a></li>
							<li><a href="#">개인정보취급방침</a></li>
							<li><a href="#">책임의 한계와 법적고지</a></li>
							<li><a href="#">게시중단요청 서비스</a></li>
							<li><a href="#">고객센터</a></li>
						</ul>
					</div>
				</form>
				<div class="copyright">
					&copy; BaekSeokUnivercity : DataBaseMasterTeam</a>.
				</div>
			</div>
		</section>
	</div>
</body>
</html>