
-- 고객
CREATE TABLE user ( 
  userid varchar(15) NOT NULL,
  userpass varchar(15) NOT NULL,
  username varchar(5) DEFAULT NULL,
  userage int(11) DEFAULT NULL,
  usersex char(5) DEFAULT NULL,
  phone char(10) DEFAULT NULL,
  usernum char(15) DEFAULT NULL,
  passportnum char(9) DEFAULT NULL,
  address varchar(30) DEFAULT NULL,
  PRIMARY KEY (userid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into user (userid, userpass, username, userage, usersex, phone, usernum, passportnum, address)  values ("aaa","aaa", "jin", 20, "female", "11", "000000-2000000", "P651", "add1");
insert into user (userid, userpass, username, userage, usersex, phone, usernum, passportnum, address)  values ("bbb", "bbb", "park", 32, "male", "22", "000000-1000000", "P345", "add2");
insert into user (userid, userpass, username, userage, usersex, phone, usernum, passportnum, address)  values ("ccc","ccc", "kim", 41, "female", "33", "000000-2000000", "P214", "add3");

-- 좌석
CREATE TABLE seat (
   seatno varchar(5) NOT NULL,
   price int(11) NOT NULL,
   seatclass char(4) DEFAULT NULL,
  PRIMARY KEY (seatno)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into seat (seatno, price, seatclass)  values ("1aa", 155, "eco");
insert into seat (seatno, price, seatclass)  values ("2aa", 135, "eco");
insert into seat (seatno, price, seatclass)  values ("3aa", 500, "first");

//항공사
CREATE TABLE airline (
  airlinename varchar(10) NOT NULL,
  country char(5) DEFAULT NULL,
  hp varchar(30) DEFAULT NULL,
  tel char(13) DEFAULT NULL,
  PRIMARY KEY (airlinename)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into airline (airlinename, country, hp, tel)  values ("ppp", "kor", "www.dfadsf.com", "000-000-0000");
insert into airline (airlinename, country, hp, tel)  values ("ooo", "jpn", "www.fsadf.com", "111-111-1111");
insert into airline (airlinename, country, hp, tel)  values ("sss", "kor", "www.adf.com", "222-222-2222");

//노선
CREATE TABLE route (
  routeno char(8) NOT NULL,
  airlinename varchar(10) NOT NULL,
  startpoint varchar(10) NOT NULL,
  finishpoint varchar(10) NOT NULL,
  stime char(5) NOT NULL,
  ftime char(5) NOT NULL,
  middlepoint varchar(10) DEFAULT NULL,
  airportno varchar(4) NOT NULL,
  PRIMARY KEY (routeno),
  FOREIGN KEY (airlinename) REFERENCES airline (airlinename) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into route (routeno, airlinename, startpoint, finishpoint, stime, ftime, middlepoint, airportno)  values ("OZ555", "ppp", "kor", "jpn", "11:00", "13:00", "null", "123");
insert into route (routeno, airlinename, startpoint, finishpoint, stime, ftime, middlepoint, airportno)  values ("OZ666", "ppp", "fcn", "sfe", "12:00", "15:00", "dfa", "456");
insert into route (routeno, airlinename, startpoint, finishpoint, stime, ftime, middlepoint, airportno)  values ("OZ777", "ooo", "eur", "kor", "13:00", "18:00", "jpn", "789");


//비행기
CREATE TABLE airplane (
  airplaneno char(10) NOT NULL,
  airplanetype varchar(15) DEFAULT NULL,
  airlinename varchar(10) NOT NULL,
  fly char(3) DEFAULT NULL,
  boardingtime char(5) DEFAULT NULL,
  PRIMARY KEY (airplaneno),
  FOREIGN KEY (airlinename) REFERENCES airline (airlinename) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into airplane (airplaneno, airplanetype, airlinename, fly, boardingtime)  values ("daa", "qwe811", "ppp", "y","10:00");
insert into airplane (airplaneno, airplanetype, airlinename, fly, boardingtime)  values ("saa", "qw215", "ooo", "n", "11:00");
insert into airplane (airplaneno, airplanetype, airlinename, fly, boardingtime)  values ("caa", "qwe879", "iii", "y", "13:00");





//예약
CREATE TABLE reservation (
  reservationno varchar(15) NOT NULL,
  boardingtime char(5) DEFAULT NULL,
  startpoint varchar(10) DEFAULT NULL,
  finishpoint varchar(10) DEFAULT NULL,
  airlinename varchar(10) NOT NULL,
  airplaneno char(10) NOT NULL,
  routeno char(8) NOT NULL,
  userid varchar(15) DEFAULT NULL,
  seatno varchar(5) NOT NULL,
  PRIMARY KEY (reservationno),
  FOREIGN KEY (userid) REFERENCES user (userid) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (routeno) REFERENCES route (routeno) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (airplaneno) REFERENCES airplane(airplaneno) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (seatno) REFERENCES seat (seatno) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (airlinename) REFERENCES airline(airlinename) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into reservation (reservationno, boardingtime, startpoint, finishpoint, airlinename, airplaneno, routeno, userid, seatno)  values ("ff55","10:00", "kor", "jpn", "ppp", "daa", "OZ666", "aaa", "2aa");
insert into reservation (reservationno, boardingtime, startpoint, finishpoint, airlinename, airplaneno, routeno, userid, seatno)  values ("dd55", "10:00", "kor", "jpn", "ppp", "daa", "OZ666", "ccc", "1aa");
insert into reservation (reservationno, boardingtime, startpoint, finishpoint, airlinename, airplaneno, routeno, userid, seatno)  values ("ee55", "13:00", "eur", "kor", "ppp", "saa", "OZ777", "bbb", "1aa");